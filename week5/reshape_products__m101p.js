use agg
db.products.aggregate([
    {$project:
     {
	 _id:0,
	 'maker': {$toLower:"$manufacturer"},
	 'details': {'category': "$category",
		     'price' : {"$multiply":["$price",10]}
		    },
	 'item':'$name'
     }
    }
])


db.products.aggregate([
    {$project:
     {
	 _id:0,
	 'pop':1,
	 'state':1
	 'city': {$toLower:"$city"},
	 'zip':"$_id"
     }
    }
])


