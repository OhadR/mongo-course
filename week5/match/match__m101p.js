use agg
db.zips.aggregate([
    {$match:
     {
	 state:"NY"
     }
    }
])

QUIZ:

db.zips.aggregate([
    {$match:
     {
	 pop:{$gt:100000}
     }
    }
])

QUIZ - sort multiple keys

db.zips.aggregate([
    {$sort:
     {
	 state:1,
	 city:1
     }
    }
])
