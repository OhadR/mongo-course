use agg
db.products.aggregate([
    {$group:
     {
	 _id: {
	     "maker":"$manufacturer"
	 },
	 categories:{$addToSet:"$category"}
     }
    }
])


QUIZ:

use agg
db.zips.aggregate([
    {$group:
     {
	 _id: "$city",
	 postal_codes:{$addToSet:"$_id"}
     }
    }
])


