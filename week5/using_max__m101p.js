use agg
db.products.aggregate([
    {$group:
     {
	 _id: {
	     "maker":"$manufacturer"
	 },
	 maxprice:{$max:"$price"}
     }
    }
])

QUIZ:

db.zips.aggregate([
    {$group:
     { _id: "$state",
	   pop:{$max:"$pop"}
     }
    }
])