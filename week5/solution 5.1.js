question:

Finding the most frequent author of comments on your blog
In this assignment you will use the aggregation framework to find the most frequent author of comments on your blog. We will be using a data set similar to ones we've used before.
Start by downloading the handout zip file for this problem. Then import into your blog database as follows:
mongoimport --drop -d blog -c posts posts.json
Now use the aggregation framework to calculate the author with the greatest number of comments.
To help you verify your work before submitting, the author with the fewest comments is Mariela Sherer and she commented 387 times.
Please choose your answer below for the most prolific comment author:


solution:

very similar to file:unwind/blog_tags.js


> use posts
> db.posts.aggregate([
    /* unwind by comments */
    {"$unwind":"$comments"},
    /* now group by authors, counting each author */
    {"$group": 
     {"_id":"$comments.author",
      "count":{$sum:1}
     }
    },
    /* sort by popularity */
    {"$sort":{"count":-1}},
    /* show me the top 10 */
    {"$limit": 10}
])