package course.exam.q7;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.IOException;
import java.util.ArrayList;

/**
 * This class encapsulates the controllers for the blog web application.  It delegates all interaction with MongoDB
 * to three Data Access Objects (DAOs).
 * <p/>
 * It is also the entry point into the web application.
 */
public class Q7Controller {

    MongoCollection<Document> albumsCollection;
    MongoCollection<Document> imagesCollection;

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            new Q7Controller("mongodb://localhost");
        }
        else {
            new Q7Controller(args[0]);
        }
    }

    public Q7Controller(String mongoURIString) throws IOException {
        final MongoClient mongoClient = new MongoClient(new MongoClientURI(mongoURIString));
        final MongoDatabase blogDatabase = mongoClient.getDatabase("exam7");

        albumsCollection = blogDatabase.getCollection("albums");
        imagesCollection = blogDatabase.getCollection("images");

        solveProblem();
    }

	private void solveProblem() {
		FindIterable<Document> iter = albumsCollection.find();
		MongoCursor<Document> cursor = iter.iterator();

		try {
    		//iterate over albums, and mark all images:

    		while(cursor.hasNext()) 
            {
            	//mark the image:
            	Document album = cursor.next();
            	ArrayList<Integer> imagesArray = album.get("images", ArrayList.class);
            	for(Integer imageId : imagesArray)
            	{
            		System.out.println(imageId);

                	Bson findFilter = Filters.eq( "_id", imageId );
                	Bson setUpdate = Updates.set("orphan", false);

            		imagesCollection.updateOne(findFilter, setUpdate);

            	}
            	//image.append("orphan", false);
//                log.info(cursor.next().toJson());
            }
    		
    		//iterate over images and delete those without the flag:
    		iter = imagesCollection.find();
    		cursor = iter.iterator();
    		while(cursor.hasNext()) 
            {
            	//mark the image:
            	Document image = cursor.next();
            	Boolean isOrphan = image.get("orphan", Boolean.class);
            	if(isOrphan == null)
            	{
            		Object imageId = image.get("_id");
            		System.out.println(imageId);
            		imagesCollection.deleteOne( Filters.eq( "_id", imageId ) );

                	Bson findFilter = Filters.eq( "_id", imageId );
                	Bson setUpdate = Updates.set("orphan", false);

            		imagesCollection.updateOne(findFilter, setUpdate);

            	}
            	//image.append("orphan", false);
//                log.info(cursor.next().toJson());
            }
    		
        	Bson sunrisesTagfindFilter = Filters.eq( "tags", "sunrises" );

        	int size = imagesCollection.find(sunrisesTagfindFilter)
        		.into(new ArrayList<Document>()).size();

    		System.out.println("result is: " + size);


    		
        } finally {
            cursor.close();
        }

		
	}
}
