/*
 * Copyright 2012-2016 MongDB, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package course;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.m101j.util.Helpers;

import org.bson.Document;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class BlogPostDAO {
	// author, body, permalink, tags, comments, date, title
    private static final String TITLE_KEY = "title";
    private static final String DATE_KEY = "date";
    private static final String COMMENTS_KEY = "comments";
    private static final String TAGS_KEY = "tags";
    private static final String PERMALINK_KEY = "permalink";
    private static final String BODY_KEY = "body";
    private static final String AUTHOR_KEY = "author";
    private static final String EMAIL_KEY = "email";
    private static final String NUM_LIKES_KEY = "num_likes";
    MongoCollection<Document> postsCollection;

    public BlogPostDAO(final MongoDatabase blogDatabase) {
        postsCollection = blogDatabase.getCollection("posts");
    }

    // Return a single post corresponding to a permalink
    public Document findByPermalink(String permalink) {

        // XXX HW 3.2,  Work Here
    	Bson filter = eq(PERMALINK_KEY, permalink);

        Document post = postsCollection.find(filter)
        		.first();

//        Helpers.printJson(post);
        // fix up if a post has no likes
        if (post != null) {
            List<Document> comments = (List<Document>) post.get("comments");
            for (Document comment : comments) {
                if (!comment.containsKey(NUM_LIKES_KEY)) {
                    comment.put(NUM_LIKES_KEY, 0);
                }
            }
        }
        return post;
    }

    // Return a list of posts in descending order. Limit determines
    // how many posts are returned.
    public List<Document> findByDateDescending(int limit) {

        // XXX HW 3.2,  Work Here
        // Return a list of DBObjects, each one a post from the posts collection
        List<Document> posts = null;

        Bson sortByDate = Sorts.descending( DATE_KEY );

        posts = (List<Document>) postsCollection.find()
        		.sort(sortByDate)
        		.limit(limit)
            	.into(new ArrayList<Document>());
        return posts;
    }

    public List<Document> findByTagDateDescending(final String tag) {
        return postsCollection.find(Filters.eq("tags", tag))
                       .sort(Sorts.descending("date"))
                       .limit(10)
                       .into(new ArrayList<Document>());
    }


    public String addPost(String title, String body, List tags, String username) {

        System.out.println("inserting blog entry " + title + " " + body);

        String permalink = title.replaceAll("\\s", "_"); // whitespace becomes _
        permalink = permalink.replaceAll("\\W", ""); // get rid of non alphanumeric
        permalink = permalink.toLowerCase();

        // XXX HW 3.2, Work Here

        String permLinkExtra = String.valueOf(GregorianCalendar
                .getInstance().getTimeInMillis());
        permalink += permLinkExtra;


        // Build the post object and insert it
        Document post = new Document();

        post.append(TITLE_KEY, title)
        	.append(AUTHOR_KEY, username)
        	.append(BODY_KEY, body)
        	.append(PERMALINK_KEY, permalink)
        	.append(TAGS_KEY, tags)
        	.append(COMMENTS_KEY, new ArrayList<Document>())
        	.append(DATE_KEY, new Date());
        
        postsCollection.insertOne(post);


        return permalink;
    }


    // Append a comment to a blog post
    public void addPostComment(final String name, final String email, final String body,
                               final String permalink) {

        // XXX HW 3.3, Work Here
        Document newComment = new Document(AUTHOR_KEY, name)
        		.append(BODY_KEY, body);

        if (email != null && !email.equals("")) {
            // if there is an email address specified, add it to the document too.
        	newComment.append(EMAIL_KEY, email);
        }
        
    	Bson postFilter = Filters.eq( PERMALINK_KEY, permalink );
    	Bson setUpdate = Updates.push(COMMENTS_KEY, newComment);

		postsCollection.updateOne(postFilter, setUpdate);
    }
    
    
    public void likePost(final String permalink, final int ordinal) {
        //
        //
        // XXX Final Question 4 - work here
        // You must increment the number of likes on the comment in position `ordinal`
        // on the post identified by `permalink`.
        //
        //
    	System.out.println(PERMALINK_KEY + ":" + permalink + ", ordinal=" + ordinal);

    	Document post = findByPermalink(permalink);
        if (post == null) 
        	return;
        
//        List<Document> comments = (List<Document>) post.get(COMMENTS_KEY);
//        Document comment = comments.get(ordinal);
//        Integer numLikes = comment.get(NUM_LIKES_KEY, Integer.class);
//        comment.put(NUM_LIKES_KEY, ++numLikes);

    	Bson postFilter = Filters.eq( PERMALINK_KEY, permalink );
    	Bson setUpdate = Updates.inc(COMMENTS_KEY + "." + ordinal + "." + NUM_LIKES_KEY, 1);

		postsCollection.updateOne(postFilter, setUpdate);
    }
}
