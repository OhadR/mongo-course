/*
 * Copyright 2015 MongoDB, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mongodb.m101j.crud;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.m101j.util.Helpers.printJson;

public class Week3_FindWithFilter {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase database = client.getDatabase("school");
        MongoCollection<Document> collection = database.getCollection("students");

        List<Document> homeworks = collection.find()
        	.into(new ArrayList<Document>());

//        for (Document cur : homeworks) {
//            printJson(cur);
//        }
//        
        for(Document doc : homeworks)
        {
        	ArrayList<Document> scores = (ArrayList<Document>) doc.get("scores");
        	//iterate over the scores of each student (there are 4 scores: quiz, exam and 2*homework)
    		double lowestHomework = Double.MAX_VALUE;
        	for(Document embeddedDoc : scores)
        	{
        		if(embeddedDoc.getString("type").equals("homework"))
        		{
        			Double score = embeddedDoc.getDouble("score");
        			if(score < lowestHomework)
        			{
        				lowestHomework = score;
        			}
        		}
        	}
        	Bson studentFilter = Filters.eq( "_id", doc.get("_id") );
        	Bson delete = Updates.pull("scores", new Document("score", lowestHomework).append("type", "homework"));

			collection.updateOne(studentFilter, delete);
		}

        client.close();
    }
}
