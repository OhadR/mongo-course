/*
 * Copyright 2015 MongoDB, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mongodb.m101j.crud;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.m101j.util.Helpers.printJson;

public class FindWithFilterTest {
    public static void main(String[] args) {
        MongoClient client = new MongoClient();
        MongoDatabase database = client.getDatabase("students");
        MongoCollection<Document> collection = database.getCollection("grades");

//        Bson filter = new Document("type", "exam")
//        		.append("score", new Document("$gt", 65) );

//        Bson filter = and(eq("type", "exam"), gt("score", 65));
        Bson filter = eq("type", "homework");
        Bson sortByStudent = Sorts.ascending("student_id");
        Bson sortByScore = Sorts.ascending("score");
        Bson combinedSort = Sorts.orderBy(sortByStudent, sortByScore);

        List<Document> homeworks = collection.find(filter)
        		.sort(combinedSort)
//        		.first()
        		.into(new ArrayList<Document>());

        for (Document cur : homeworks) {
            printJson(cur);
        }
        
        for(int i = 0; i < homeworks.size(); ++i)
        {
        	Document doc = homeworks.get(i);
//        	printJson(doc);
/*        	if(i % 2 == 0)
        	{
	        	Object id = doc.get("_id");
				collection.deleteOne(eq("_id", id));
				System.out.println("deleting id = " + id);
        	}
*/        }

        long count = collection.count(filter);
        System.out.println();
        System.out.println(count);
        
        client.close();
    }
}
