Layout
---
**blog_accepting_posts_and_comments** - week 3 project. Not in use, as "create_blog_authors" was modified with changes.

**create_blog_authors** - the project I worked with. Every assignment I committed/pushed to this project, so you can see the history.

**spark-freemarker-mongodb** - sample project that integrates spark, freemaker, and mongodb driver.

**M101J_ohadsSparkFreemaker** - sample project for spark and freemaker.

**week6\examples** - sample code for replication

db.<coll>.insertOne({"a" : "b", ...});

db.<coll>.find()

db.<coll>.find().count()

db.<coll>.insertMany([{"a" : "b", ...},{}]);

db.<coll>.drop()

cursonrs & projections ("Reading Documents")

READING Documents
===
https://www.youtube.com/watch?v=yP0Islq0kBo

this query finds a doc where 'writers' is an array, that contains EXACTLY the specified members, with the same order:

> db...find({ "writers": ["Joel", "Ethan"]})

this query finds a doc where 'actors' is an array, that (just) contains the specified member:

> db...find({ "actors" : "Jeff Bridges" })

find all events of type "File" OR "Email":

> db.getCollection('events').find({"type":{$in:["File","Email"]}})

HW 2.2  
------
Now it's your turn to analyze the data set. Find all exam scores greater than or equal to 65, and sort those scores from lowest to highest.

What is the student_id of the lowest exam score above 65?


> db.grades.find({ score: { $gt: 65 } }).count()

303

> db.grades.find({ score: { $gt: 65 } , type: "exam"} ).count()

76


HW 2.3
------

Now let us sort the students by student_id , and score, while also displaying the type to then see what the top five docs are:

> db.grades.find( { }, { 'student_id' : 1, 'type' : 1, 'score' : 1, '_id' : 0 } ).sort( { 'student_id' : 1, 'score' : 1 } ).limit( 5 )


To verify that you have completed this task correctly, provide the identity of the student with the highest average in the class with 
following query that uses the aggregation framework. The answer will appear in the _id field of the resulting document.

> db.grades.aggregate( { '$group' : { '_id' : '$student_id', 'average' : { $avg : '$score' } } }, { '$sort' : { 'average' : -1 } }, { '$limit' : 1 } )


$set operator add/changes fields but other fields remain the same.

```
new Document("$set", new Document("examiner", "Jones"))

collection.updateOne( eq("x":5), new Document("$set", new Document("x", 20).append( "updated":true )) )

collection.updateOne( eq("x":5), Updates.combine( Updates.set("x", 20), Updates.set( "updated":true )) )
```

> mongoimport --drop -d students -c grades grades.json
> mongoimport --drop --db students --collection grades grades.json

> mongorestore -d db_name -c collection_name /path/file.bson

HW 2.5  Which of the choices below is the title of a movie from the year 2013 that is rated PG-13 and won no awards?
------
> use video
> db.movies.find().count()
3365

> db.movieDetails.find().count()
2295
> db.movieDetails.find({"year": 2013, "rated": "PG-13"}).count()
11
> db.movieDetails.find({"year": 2013, "rated": "PG-13", "awards.wins": 0})


HW 2.6 how many movies list "Sweden" second in the the list of countries.
------
> db.movieDetails.find({"countries.1": "Sweden"}).count()

![mongo-server](mongo-server.jpg)


One to Many
----
* One to Many - if "many" is huge, then we better use "true linking", similar to foreign keys (city:NYC...)
* One to Few - embeding the "few" in the main document is better parctice, in cases like post and comments of that post. we embed the 
comments as an array in the post docuemtn.

Many to Many
----
Few to Few - we can use linking, or embedding. e.g. books/authors, students/teachers. embedding not good, because of duplications, and anomalies if 
data is not consistent (changes). so better make both books and authors first-class objects.

Multikey Indexes
-----
create a multikey index:
> db.students.ensureIndex({'teachers':1})

use the multikey index:
> db.students.find({ 'teachers':{ $all:[0,1] } })

(find all the students that have both teacher 0 and 1 in their 'teachers' array.)

When to denormalize
----
* 1:1 - embed
* 1:many - embed (from the many to the one)
* many:many - link


MORPHIA
---
M101J Morphia
https://www.youtube.com/watch?v=DB67UpBitM0

M101J CRUD operations with Morphia
https://www.youtube.com/watch?v=Ts96-cdUeko


Performance
===
impact latency and throuput: 
* add indexes to collections, 
* distribute the load across servers using sharding

Storage Engine
---
the application talks to the MongoDB (using the driver), and the MongoDB writes (persists) the data to
the disk using the Storage Engine. This engine can decide to optimize and use the memory (much faster).
Pluggable Storage Engine - the SE can be replaced, and same in cars, if you replace the engine
the performance of the car vary.

MMAP
---
https://www.youtube.com/watch?v=os3591KviNM

Maps the allocated size on disk to a VM Virtual Memory (like cache)

1. *Collection* Level Locking - Multiple readers, Single writer.
2. In place updates
3. Power-of-2 sizes.


WiredTiger
---

1. Document level concurrency
2. Compression - of data and indexes.
3. No inplace updates. Copies the data to another place in memory, but allows to run without locks (much faster).

> mkdir WT

> mongod -dbpath WT -storageEngine wiredTiger

Indexes
---
Writes are slower, but Reads are MUCH faster. 
When inserting tons of documents, it is better to insert them WITHOUT index, and add it after all
is inserted. because inserting with index forces to "re-index" every time.

Creating an index
---
https://www.youtube.com/watch?v=xi2gtzZez6Q
> db.students.createIndex({studentId:1})

>...explain(true).find(...)
passing true, you can see how many documents were examined ("docsExamined").
you can see what indexes were in use.

compound index:
> db.students.createIndex({studentId:1, classId:-1});

classId:-1 means *descending*. the studentId is *ascending*. Note: this affects sorting.


>db.students.getIndexes();

>db.students.dropIndex({student_id:1});

Multikey (arrays) and dot notation
---

> db.students.createIndex({'scores.score':1});

and then: search for documents with any score above 99:
> db.students.find({scores.score:{'$gt':99}});

search for a doc where only exam is above 99:
> db.students.find({'scores': {$elemMatch: {type:'exam', score:{'$gt':99}}}});

$elemMatch operator search documents with array field, that matches AT LEAST one element that matches

Note:
> db.students.find({'$and': [{'scores.type':'exam'}, {'scores.score':{'$gt':99}}]});

will return scores higher than 99, but not necessariliy the exam's score!


> db.students.createIndex({studentId:1}, {unique:true})


Quiz: Please provide the mongo shell command to create a unique index on student_id, class_id, ascending for the collection students.
> db.students.createIndex({student_id:1, class_id:1}, {unique:true});


> db.students.createIndex({studentId:1}, {unique:true, sparse:true})

-means that it allows and entry without the index field (and it will not be indexed)

advantage: uses a lot less space.

Indexes - foreground vs background: https://www.youtube.com/watch?v=AchmKNj2qhw

Foreground: fast; blocks writers and readers.
Background: slow, does not block.

Explain https://www.youtube.com/watch?tv=WxXVun6bZ20
---
Query Planner (default)
Execution Stats

> var exp = db.example.explain("executionStats");

> exp.find({a:17, b:55});

All-plans Execution
> var exp = db.example.explain("allPlansExecution");

Choosing an Indexes
---
when a query comes in, Mongo decides which index to use, based on the shape of the query: what fields
are searched, is it for sorting, etc. Mongo uses cache that can be evicted where:
* threshold writes
* rebuild of indexes
* an index was added/removed
* restart

if there are 3 relevant indexes for a certain query, mongo runs 3 different threads for each index (AKA 'query plan'), 
and checks which one can return results faster.

> db.students.stats()

- gets the index size. equivalent to :

> db.students.toalIndexSize();

**MAKE SURE THE INDEX SIZE FITS MEMORY** so mongo does not have to go to disk regularly.

Geo Index
---
the document should include something like: 'location':[x,y]

and then:
> db...ensureIndex({"location":'2d', type:1});

> db...find({location:{$near:[x,y]}});

**Quiz:** Suppose you have a 2D geospatial index defined on the key location in the collection places. Write a query that will find the closest three places (the closest three documents) to the location 74, 140.

> db.places.find({location:{$near:[74,140]}}).limit(3);

Geo Spherical
---

> db...ensureIndex({'location':'2dsphere', type:1});

> db...find({location:{$near: {$geometry: {type: "Point", coordinates:[x,y]}, $maxDistance:2000}}});

**Quiz:** What is the query that will query a collection named "stores" to return the stores that are within 1,000,000 meters of the location 
latitude=39, longitude=-130? Type the query in the box below. Assume the stores collection has a 2dsphere index on "loc" and please use the "$near" 
operator. Each store record looks like this:

> db.stores.find({loc:{$near: {$geometry: {type: "Point", coordinates:[-130,39]}, $maxDistance:1000000}}});

Note: mongo expects longitude and then latitude (opposite from google maps)

Text Indexes
---

> db.sentences.ensureIndex({'words':'text'});

> db.sentences.find({{$text: {$search:'dog'}}});

this one will not find anything:
> db.sentences.find('words':'dog'); 

> db.sentences.find({$text: {$search:'dog love ohad'}}, {score:{$meta: 'textScore'}}).sort({score:{$meta:'textScore'}});

**Quiz:** In general, which of the following rules of thumb should you keep in mind when building compound indexes? Check all that apply. For this question, use the following definitions:
equality field: field on which queries will perform an equality test
sort field: field on which queries will specify a sort
range field: field on which queries perform a range test

Equality --> Sort --> Range

HW 4.3
------
> db.posts.explain('executionStats').find({"tags": 'crab'}).sort( { 'date' : 1 } ).limit( 10 )

> db.posts.createIndex({ tags : 1 })

> db.posts.getIndexes()

> db.posts.explain('executionStats').find({"permalink": 'crab'})

> db.posts.createIndex({ permalink : 1 })

> db.posts.explain('executionStats').find().sort( { 'date' : 1 } ).limit( 10 )

> db.posts.createIndex({ date : -1 })



HW 4.4
---
importing:
   
   c:\> mongoimport --drop -d m101 -c sysprofile sysprofile.json

the query:

> db.sysprofile.find({ 'ns':'school2.students' }).sort({ 'millis':-1 }).limit(1).pretty()

the answer is "millis" : 15820


Aggregation
===

> use agg
> db.products.aggregate([
    {$group:
     {
	 _id:"$manufacturer", 
	 num_products:{$sum:1}
     }
    }
])


$project - reshape - 1:1
$match   - filter  - n:1
$group   - aggregate - n:1
$sort    - sort      - 1:1 (comes in/out, different order)
$skip    - skips     - n:1
$limit   - limits    - n:1
$unwind  - normalize - 1:n (if i have doc with array 'tags' with 3 items, i end with 3 documents)
$out     - output    - 1:1
$geonear, $redact


see solutions for week 5 under "week 5" directory.

persistency:

'w' value represents mongo's acknoledge for writes. How many node do we wait for, before we move on.
'j' value (true/false) represents whether data was written from journal to disk.
the default is 'w'=1, 'j'=false. it is fast, but small window of volnurability (in case where server crashes without writing journal to disk).
'w'=1, 'j'=true: slower.
'w'=0: not recommended. do not wait for acknoledge for writes.
'wtimeout': how long do we wait.

These values can be set in connection-level, collection, or replica-set. The replica-set values will be the defaults for all connections, if there 
are no other values. 
'w=majority' means to wait for the ack from the majority of nodes.

Replication
==
* Availability
* Fault tolerance

Types of Replica Set Nodes
---
* Regular - stores data. can be Primary.
* Arbiter - just for voting a new Primary.
* Delayed - cannot be Primary. can vote a new Primary.
* Hidden - for analytics. cannot be Primary. (can vote...)

creating a replica set:
---
https://www.youtube.com/watch?v=flCFVFBRsKI

each mongo has its own oplog. they all synced with the Primary. so if there was a write to the Primary, the Secondaries are reading its
oplog and applying same operations to the Secondaries.

writes are done ONLY TO THE PRIMARY. Writes to the Secondaries will fail.

Replication suppoers mixed-mode storage engines (mmapvq1, wiredTiger).


Connecting to a Replica Set from Java Driver:
https://www.youtube.com/watch?v=701LZygtnK0

rs.help()
rs.conf()
rs.slaveOk()
rs.stepDown()


Read Preference
---
* Primary (use the P only. default)
* Primary Preferred (prefer the P, but if unavailable, use the 2ndary)
* Secondary (read only from Secondaries. We get "Eventually Consistent Read", because only eventually the data is written to all Secondaries)
* Secondary Preferred
* Nearest (read from closest, by ping time)

Sharding
===
*mongos* is a router that routes the query to the specific shard. Each shard is a mongod with a replica set.

If a query specify a shard-key, then mongos checks which shard is mapped to this shard-id and routes the query. If there is no shard-key in the query,
then there is an algorithm to select shard. if the query is "find()", for example, then mongos checks ALL the replica-sets and sends the response to client.

Question: If the shard key is not included in a find operation and there are 4 shards, each one a replica set with 3 nodes, how many nodes will see the find operation?

Answer: The answer is 4. Since the shard key is not included in the find operation, mongos has to send the query to all 4 of the shards. Each shard has 3 replica-set members, but only one member of each replica set (the primary, by default) is required to handle the find.

Implications of Sharding on Development
---
* each document includes the shard key.
* shard-key is immutable.
* index that starts with the shard-key.
* shard-key is specified, or multi (goes to multiple nodes).
* no shard-key => scather gather. (expensive)
* no unique key, unless part of the shard-key

Which of the following statements are true about choosing and using a shard key?
Check all that apply:

- [ ] The shard key must be unique
- [x] There must be a index on the collection that starts with the shard key.
- [x] MongoDB can not enforce unique indexes on a sharded collection other than the shard key itself, or indexes prefixed by the shard key.
- [x] Any update that does not contain the shard key will be sent to all shards.
- [x] Any single update that does not contain the shard key or _id, will result in an error.
- [ ] You can change the shard key on a collection if you desire.



Exam
===

1
---
> mongorestore --port 27017 -d enron -c messages messages.bson

> db.messages.find({ 'headers.From': "andrew.fastow@enron.com" }).count()

Query an Array: https://docs.mongodb.com/manual/tutorial/query-arrays/

> db.messages.find({ 'headers.From': "andrew.fastow@enron.com", 'headers.To': "jeff.skilling@enron.com" }).count()

same as:
> db.messages.find({'$and': [{'headers.From': "andrew.fastow@enron.com"}, {'headers.To':"jeff.skilling@enron.com"}]}).count()

2
---
> db.messages.aggregate([ { $unwind: "$headers.To" }])

> db.messages.aggregate([
  {$project: {"from": "$headers.From", "to": "$headers.To"}},
  {$unwind: "$to"},
  {$group: {_id: {id: "$_id", from: "$from"}, to: {$addToSet: "$to"}}},
  {$unwind: "$to"},
  {$group: {_id: {from: "$_id.from", to: "$to"}, total: {$sum: 1}}},
  {$sort: {total: -1}},
  {$limit: 1}
])

one liner:

> db.messages.aggregate([  {$project: {"from": "$headers.From", "to": "$headers.To"}},  {$unwind: "$to"},  {$group: {_id: {id: "$_id", from: "$from"}, to: {$addToSet: "$to"}}},  {$unwind: "$to"},  {$group: {_id: {from: "$_id.from", to: "$to"}, total: {$sum: 1}}},  {$sort: {total: -1}},  {$limit: 1}])

3
---

headers.Message-ID
db.messages.aggregate([  {$match: { "headers.Message-ID" : "<8147308.1075851042335.JavaMail.evans@thyme>"}},  {$project: {"to": "$headers.To", "Message-ID": "$headers.Message-ID"}}])

> db.messages.updateMany({ 'headers.Message-ID': "<8147308.1075851042335.JavaMail.evans@thyme>" }, { $push: { "headers.To": "mrpotatohead@mongodb.com"} })

Or:

> db.messages.update({"headers.Message-ID": "<8147308.1075851042335.JavaMail.evans@thyme>"}, {$addToSet: {"headers.To": "mrpotatohead@mongodb.com"}})

4
---

5
---

6
---
...Now suppose that basic inserts into the collection, which only include the last name, first name and student_id, are too slow (we can't do enough of them per second from our program). What could potentially improve the speed of inserts. Check all that apply.

- [ ] Add an index on last_name, first_name if one does not already exist.
- [x] Remove all indexes from the collection, leaving only the index on _id in place (the reads will be much slower, but this will significantly improve the writes)
- [ ] Provide a hint to MongoDB that it should not use an index for the inserts
- [x] Set w=0, j=0 on writes - w=0 means we do not ask for ack from mongo for writes. so it will be much faster, even though not recommende.
- [ ] Build a replica set and insert data into the secondary nodes to free up the primary nodes.

7
---
the import command:

> mongoimport --drop -d exam7 -c albums albums.json -c images images.json
> mongoimport -d exam7 -c images images.json

> db.images.find({ "tags" : "sunrises" }).count()

but in the source, i wrote also:

```
Bson sunrisesTagfindFilter = Filters.eq( "tags", "sunrises" );

int size = imagesCollection.find(sunrisesTagfindFilter)
	.into(new ArrayList<Document>()).size();

System.out.println("result is: " + size);
```