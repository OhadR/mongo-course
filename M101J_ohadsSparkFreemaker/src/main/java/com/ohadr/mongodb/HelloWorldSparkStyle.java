package com.ohadr.mongodb;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class HelloWorldSparkStyle {
	public static void main(String[] args)
	{
		Spark.get("/", new Route() {

			@Override
			public Object handle(Request request, Response response) throws Exception {
				return "Hello from Spark!";
			}
		});

		Spark.get("/test", new Route() {

			@Override
			public Object handle(Request request, Response response) throws Exception {
				return "this is a test";
			}
		});

		Spark.get("/echo/:thing", new Route() {

			@Override
			public Object handle(Request request, Response response) throws Exception {
				return request.params(":thing");
			}
		});
	}
}
