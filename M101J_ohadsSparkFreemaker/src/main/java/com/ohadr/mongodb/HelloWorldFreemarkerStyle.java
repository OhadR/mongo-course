package com.ohadr.mongodb;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class HelloWorldFreemarkerStyle {
    public static void main( String[] args ) throws IOException, TemplateException
    {
    	Configuration configuration = new Configuration();
    	configuration.setClassForTemplateLoading(
    			HelloWorldFreemarkerStyle.class,
    			"/" //pathPrefix
    			);
    	
    	Template template = configuration.getTemplate("hello.ftl");
    	StringWriter writer = new StringWriter();
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("name", "ohad");
    	
    	template.process(map, writer);
    	
    	System.out.println(writer);
    }

}
