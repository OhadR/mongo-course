package com.ohadr.mongodb;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class HelloWorldSparkFreemarkerStyle {
    public static void main( String[] args ) throws IOException, TemplateException
    {
    	final Configuration configuration = new Configuration();
    	configuration.setClassForTemplateLoading(
    			HelloWorldSparkFreemarkerStyle.class,
    			"/" //pathPrefix
    			);
    	Spark.get("/", new Route() {

			@Override
			public Object handle(Request request, Response response) throws Exception {

		    	Template template = configuration.getTemplate("hello.ftl");
		    	StringWriter writer = new StringWriter();
		    	Map<String, Object> map = new HashMap<String, Object>();
		    	map.put("name", "ohad");
		    	
		    	template.process(map, writer);
		    	
		    	System.out.println(writer);

				return writer;
			}
		});
    }
}
